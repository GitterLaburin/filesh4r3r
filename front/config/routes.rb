Rails.application.routes.draw do
  root to: 'main#index'

  match 'auth',     to: 'authentication#index',   via: [:get,:post]
  get 'signup',     to: 'authentication#signup'
  match 'user',     to: 'users#index',            via: [:get,:post]
  match 'files',    to: 'files#index',            via: [:get,:post]

  get 'liveness',	to: 'liveness#liveness'
end
