class JsonWebToken
  # secret to decode token

  def self.decode(token)
    # cant store key as ruby object in yaml file
    public_key = Rails.application.secrets.public_key_base
    public_key = OpenSSL::PKey::RSA.new(public_key)
    # get payload; first index in decoded Array
    body = JWT.decode(token, public_key, true, algorithm:'RS256')[0]
    HashWithIndifferentAccess.new body
      # rescue from expiry exception
  rescue JWT::ExpiredSignature, JWT::VerificationError => e
    # raise custom error to be handled by custom handler
    raise ExceptionHandler::InvalidToken, e.message
  end
end
