class AuthorizeApiRequest
  def initialize(cookies = {})
    @cookies = cookies
  end

  def call()
    {
      user: user()
    }
  end

  private

  attr_reader :cookies

  def user()
    @user ||= decoded_auth_token()
  end

  def decoded_auth_token()
    token = auth_cookie()
    @decoded_auth_token ||= JsonWebToken.decode(token)
  end

  def auth_cookie
    if cookies['session'].present?
      return cookies['session']
    end
      raise(ExceptionHandler::MissingToken, Message.missing_token)
  end
  rescue
    raise(ExceptionHandler::InvalidToken, Message.invalid_token)
end
