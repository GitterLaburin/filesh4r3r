class FilesController < ApplicationController
  def index
    if @current_user['is_admin'] != true
      flash.now[:error] = "File Upload is Temporary Disable for Users"
      return
    end
    suffix = ''
    if request.get?
      if params.has_key?(:filename)
        path = '/download'
        suffix = Base64.encode64(params['filename']).strip
      else
        path = '/list'
      end
    else
      path = '/upload'
    end
    if request.headers['Content-Type'].present?
      ct = request.headers['Content-Type']
    else
      ct = 'application/x-www-form-urlencoded'
    end
    Net::HTTP.start(Rails.application.secrets.files_api_addr, Rails.application.secrets.files_api_port) { |http|
      res = http.send_request(request.method, '/api' + path + '/' + @current_user['user_id'].to_s + '/' + suffix, request.raw_post, {'Content-Type'=>ct})
      case res
        when Net::HTTPSuccess
          if request.get?
            if params.has_key?(:filename)
              send_data res.body, filename: params['filename']
            else
              @files = JSON.parse(res.body)["files"]
            end
          else
            flash[:notice] = 'File Uploaded'
            redirect_to files_path
          end
        when Net::HTTPNotFound
          flash.now[:error] = "File not found"
        else
          flash.now[:error] = JSON.parse(res.body)["message"]
      end
    }
  end
end
