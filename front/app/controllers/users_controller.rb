class UsersController < ApplicationController
  def index
    req_params = params
    if request.post?
      req_params = user_update_params  
    end
    req_params['id'] = @current_user['user_id']
    Net::HTTP.start(Rails.application.secrets.auth_api_addr, Rails.application.secrets.auth_api_port) { |http|
      res = http.send_request(request.method, '/user', req_params.as_json.to_query)
      case res
      when Net::HTTPSuccess
        if request.get?
          @user_data ||= JSON.parse(res.body)
        else
          auth_token ||= JSON.parse(res.body)["token"]
          cookies[:session] = auth_token
        end
      else
        flash.now[:error] = JSON.parse(res.body)["message"]
      end
    }
  end

  private

  def user_update_params
    params.permit(:username, :password)
  end
end
