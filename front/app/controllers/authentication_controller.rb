require 'net/http'
require 'json'

class AuthenticationController < ApplicationController
  skip_before_action :authorize_request
  def index
    if request.post?
      if params.has_key?(:signup)
        path = '/user'
      else
        path = '/auth'
      end
      Net::HTTP.start(Rails.application.secrets.auth_api_addr, Rails.application.secrets.auth_api_port) { |http|
        res = http.send_request(request.method, path, auth_params.as_json.to_query)
        case res
          when Net::HTTPSuccess
            auth_token ||= JSON.parse(res.body)["token"]
            cookies[:session] = auth_token
          when Net::HTTPUnauthorized
            flash.now[:error] = "Wrong Username or Password"
          when Net::HTTPConflict
            flash.now[:error] = "User Already Exists"
          else
            flash.now[:error] = JSON.parse(res.body)["message"]
        end
      }
    end
    begin
      if authorize_request.present?
        redirect_to user_path
      end
    rescue
      nil
    end
  end

  def signup
    begin
      if authorize_request.present?
        redirect_to user_path
      end
    rescue
      nil
    end
  end

  private

  def auth_params
    params.permit(:username, :password)
  end
end
